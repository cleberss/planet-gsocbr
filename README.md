**Planeta GSoC Brasil**

Este repositório possui o código-fonte responsável pela geração do [Planeta GSoc Brasil](http://gsocbr.org)

Se você é um estudante participante do GSoC 2015, por favor crie uma Issue em **Incidentes** no menu ao lado, contendo seu nome, a url do seu blog e a url para um feed rss, preferencialmente de uma categoria ou tag dedicada a posts sobre o GSoC.